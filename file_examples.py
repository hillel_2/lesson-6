file_data = open('log.txt')
content = file_data.read()
print(content)
file_data.close()

with open('log.txt', 'r+') as file_data:
    print(file_data.tell())
    file_data.write('Before\n')
    content = file_data.read()
    print(file_data.tell())
    file_data.write('After\n')
    print(content)


print('Append data')
with open('log.txt', 'a+') as file_data:
    print(file_data.tell())
    file_data.write('Append before\n')
    file_data.seek(400)
    content = file_data.read()
    print(file_data.tell())
    file_data.write('Append after\n')
    print(content)