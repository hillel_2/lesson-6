class Person:
    def __init__(self, name, document, phone):
        self._name = name
        self.__document = document
        self._phone = phone
        self.purpose = 'Find your own place in the Earth'

    def _make_introduction(self):
        return f'{self._name}, {self._phone}'

    def introduce(self):
        print(self.__document)
        print(self._make_introduction())


teacher = Person('Sasha', 'Multipasport', '+19999999')
teacher._name = 12345
teacher.introduce() # Person.introduce(teacher)
teacher.__document = "Щось"
teacher.introduce() # Person.introduce(teacher)

class Animal:
    CLASS_ATTR = 10

    def __init__(self, attr):
        self.attr = attr

    def sound(self):
        print(self.attr)


class Dog(Animal):
    def __init__(self, attr):
        super().__init__(attr)

    def sound(self):
        print('Гав, ага')


print(teacher.__dict__)
teacher.__dict__['new'] = 1234
print(teacher.new)