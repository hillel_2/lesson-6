class CustomOpen:
    def __init__(self, name, mode: str = 'r'):
        self._name = name
        self._mode = mode
        self._file = None

    def __enter__(self):
        print(f'Create and return {self._name}')
        self._file = open(self._name, self._mode)
        return self._file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(f'Close {self._name}')
        if self._file is not None:
            self._file.close()

read_log = CustomOpen('log.txt', 'a')

with read_log as custom_open:
    data = custom_open.read()
    print(data)
print('End')
