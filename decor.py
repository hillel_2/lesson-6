from typing import Union
from functools import wraps
from datetime import datetime

def decorator(func):
    def wrapper(*args, **kwargs):
        print(f'Calling {func.__name__}')
        return func(*args, **kwargs)
    return wrapper


#@decorator
def add(a: Union[int, float], b: Union[int, float]) -> Union[int, float]:
    return a + b

print('Before wrapping')
help(add)


add = decorator(add)
print(add(1, 2.0))
print('After wrapping')
help(add)

def extract(a: int, b: int) -> int:
    return a - b

renamed_extract = extract
print(renamed_extract(1, 2))

def fixed_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print(f'Calling {func.__name__}')
        return func(*args, **kwargs)
    return wrapper

def mul(a: Union[int, float], b: Union[int, float]) -> Union[int, float]:
    return a * b

print('Before wrapping')
help(mul)
mul = fixed_decorator(mul)
print('After wrapping')
help(mul)


def logger_decor(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        with open('log.txt', 'a') as f:
            f.write(f'[{datetime.now()}] Calling {func.__name__}\n')
        return func(*args, **kwargs)
    return wrapper


@logger_decor
def div(a: int, b: int) -> float:
    return a / b

div(10, 2)


def logger_decor_custom(logger_file: str):
    def logger_decor(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with open(logger_file, 'a') as f:
                f.write(f'[{datetime.now()}] Calling {func.__name__}\n')
            return func(*args, **kwargs)
        return wrapper
    return logger_decor

"""
logger_decor_for_pow = logger_decor_custom('pow.txt')
@logger_decor_for_pow
"""
@logger_decor_custom('pow.txt')
def power(base, pow):
    return base ** pow

power(2, 5)
@logger_decor_custom('pow.txt')
def power(base, pow):
    return base ** pow

print(id(logger_decor_custom('pow.txt')(power)))
print(id(logger_decor_custom('pow.txt')(power)))

def реальність(func):
    def wrapper(*args, **kwargs):
        print('Ага, щас.')
    return wrapper

@реальність
def взять_київ_за_3_дні():
    print('День победи')

взять_київ_за_3_дні()

